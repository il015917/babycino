class TestBugJ1 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
//Allow y to be a boolean expression
    public int f() {
      int x;
      boolean y;

      x = 2;
      y = false;

      x += y;

      return x;

    }

}
