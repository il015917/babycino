class TestBugJ2 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}
//Assigning y to x instead of assigning x + y to x
class Test {

    public int f() {
	     int x;
       int y;

       x = 1;
       y = 4;

       x += y;

       return y;
    }

}
