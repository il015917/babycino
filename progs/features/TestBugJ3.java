class TestBugJ3 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {
//x is not modified
    public int f() {
	     int x;
       int y;

       x = 5;
       y = 1;

       x += y;

       return x;
    }

}
